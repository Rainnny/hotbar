package me.rainnny.example.listener;

import me.rainnny.hotbar.Button;
import me.rainnny.hotbar.Hotbar;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import static org.bukkit.Bukkit.getPluginManager;

/*
 * Date Created: May 06, 2020
 *
 * @author Braydon
 *
 */
public class PlayerListener implements Listener {
    public PlayerListener(JavaPlugin plugin) {
        getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    private void onJoin(PlayerJoinEvent event) {
        new ExampleHotbar(event.getPlayer()).give();
    }

    private static class ExampleHotbar extends Hotbar {
        public ExampleHotbar(Player player) {
            super(player);
        }

        @Override
        public void give() {
            // Example item with interact event
            add(new Button(0, new ItemStack(Material.DIRT), event -> {
                getPlayer().sendMessage("Clicked the dirt!");
            }));

            // Example item with no interact event, and only a entity interact event
            add(new Button(4, new ItemStack(Material.BOOK), null, event -> {
                getPlayer().sendMessage("you clicked an entity: " + event.getRightClicked().getType().name());
            }));

            // Example item that you can move in your inventory
            add(new Button(7, new ItemStack(Material.DIAMOND), null, null).setMoveable(true));

            // Example item that you can drop
            add(new Button(8, new ItemStack(Material.EMERALD), null, null).setDroppable(true));
        }
    }
}