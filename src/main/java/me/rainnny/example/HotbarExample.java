package me.rainnny.example;

import me.rainnny.example.listener.PlayerListener;
import org.bukkit.plugin.java.JavaPlugin;

/*
 * Date Created: May 06, 2020
 *
 * @author Braydon
 *
 */
public class HotbarExample extends JavaPlugin {
    @Override
    public void onEnable() {
        new PlayerListener(this);
    }
}