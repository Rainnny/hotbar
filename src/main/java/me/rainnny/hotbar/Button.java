package me.rainnny.hotbar;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.function.Consumer;

/*
 * Date Created: May 06, 2020
 *
 * @author Braydon
 *
 */
@Getter
public class Button {
    private final int slot;
    private final ItemStack item;
    private boolean moveable, droppable;

    private final Consumer<PlayerInteractEvent> interactEvent;
    private final Consumer<PlayerInteractEntityEvent> entityInteractEvent;

    public Button(int slot, ItemStack item) {
        this(slot, item, null, null);
    }

    public Button(int slot, ItemStack item, Consumer<PlayerInteractEvent> interactEvent) {
        this(slot, item, interactEvent, null);
    }

    public Button(int slot, ItemStack item, Consumer<PlayerInteractEvent> interactEvent, Consumer<PlayerInteractEntityEvent> entityInteractEvent) {
        this.slot = slot;
        this.item = item;
        this.interactEvent = interactEvent;
        this.entityInteractEvent = entityInteractEvent;
    }

    public Button setMoveable(boolean moveable) {
        this.moveable = moveable;
        return this;
    }

    public Button setDroppable(boolean droppable) {
        this.droppable = droppable;
        return this;
    }
}