package me.rainnny.hotbar;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/*
 * Date Created: May 06, 2020
 *
 * @author Braydon
 *
 */
@Getter
public abstract class Hotbar {
    private final Player player;
    private final List<Button> buttons = new ArrayList<>();

    public Hotbar(Player player) {
        this.player = player;
        HotbarManager.hotbars.put(player, this);
    }

    public abstract void give();

    public void add(Button button) {
        buttons.add(button);
        getPlayer().getInventory().setItem(button.getSlot(), button.getItem());
        getPlayer().updateInventory();
    }
}