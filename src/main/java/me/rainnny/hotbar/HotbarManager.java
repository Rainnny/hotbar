package me.rainnny.hotbar;

import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;

import static org.bukkit.Bukkit.getPluginManager;

/*
 * Date Created: May 06, 2020
 *
 * @author Braydon
 *
 */
public class HotbarManager implements Listener {
    protected static final HashMap<Player, Hotbar> hotbars = new HashMap<>();

    public HotbarManager(JavaPlugin plugin) {
        getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    private void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (!hotbars.containsKey(player))
            return;
        ItemStack item = event.getCurrentItem();
        if (item == null || (item.getType() == Material.AIR))
            return;
        Button button = hotbars.get(player).getButtons().stream().filter(b -> b.getItem().isSimilar(item))
                .findFirst().orElse(null);
        if (button == null)
            return;
        if (!button.isMoveable())
            event.setCancelled(true);
    }

    @EventHandler
    private void onDrop(PlayerDropItemEvent event) {
        Player player = event.getPlayer();
        if (!hotbars.containsKey(player))
            return;
        Button button = hotbars.get(player).getButtons().stream().filter(b -> b.getItem().isSimilar(event.getItemDrop().getItemStack()))
                .findFirst().orElse(null);
        if (button == null)
            return;
        if (!button.isDroppable())
            event.setCancelled(true);
    }

    @EventHandler
    private void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (!hotbars.containsKey(player))
            return;
        Button button = hotbars.get(player).getButtons().stream().filter(b -> b.getItem().isSimilar(event.getItem()))
                .findFirst().orElse(null);
        if (button == null || (button.getInteractEvent() == null))
            return;
        button.getInteractEvent().accept(event);
    }

    @EventHandler
    private void onEntityInteract(PlayerInteractEntityEvent event) {
        Player player = event.getPlayer();
        if (!hotbars.containsKey(player))
            return;
        ItemStack item = player.getItemInHand();
        if (item == null || (item.getType() == Material.AIR))
            return;
        Button button = hotbars.get(player).getButtons().stream().filter(b -> b.getItem().isSimilar(item))
                .findFirst().orElse(null);
        if (button == null || (button.getEntityInteractEvent() == null))
            return;
        button.getEntityInteractEvent().accept(event);
    }

    @EventHandler
    private void onQuit(PlayerQuitEvent event) {
        hotbars.remove(event.getPlayer());
    }
}